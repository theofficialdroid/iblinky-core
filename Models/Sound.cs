﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tobii.Blinky.Models
{
    public class Sound
    {
        public string Name { get; set; }
        public string path { get; set; }

        // Change media type to something that allows media volume to be altered, alternatively write >>
        // >> service provider to rewrite WAV, MP3 format media to increase sound wave, db-! output >>
        // >> in real-time on a back-thread.
        public System.Media.SoundPlayer player { get; set; }
    }
}
