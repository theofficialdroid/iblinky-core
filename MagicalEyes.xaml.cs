﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Tobii.Blinky
{
    /// <summary>
    /// Interaction logic for MagicalEyes.xaml
    /// </summary>
    public partial class MagicalEyes : Window
    {

        private SolidColorBrush EyeColour;

        public SolidColorBrush Eyecolour
        {
            get { return EyeColour; }
            set { 
                EyeColour = value;
                OnPropertyChanged();
            }
        }

        private void OnPropertyChanged()
        {
            trackerBlinkStatusLeft.Fill = EyeColour;
            trackerBlinkStatusRight.Fill = EyeColour;
        }

        public MagicalEyes()
        {
            InitializeComponent();
        }
    }
}
