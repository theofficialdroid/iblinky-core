﻿<mah:MetroWindow x:Class="Tobii.Blinky.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mah="clr-namespace:MahApps.Metro.Controls;assembly=MahApps.Metro"
        xmlns:iconPacks="http://metro.mahapps.com/winfx/xaml/iconpacks"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:Tobii.Blinky"
        mc:Ignorable="d"
        Title="Controller" 
        Height="646"
        Width="600" 
        MinWidth="430"
        GlowBrush="{DynamicResource MahApps.Brushes.Accent}"
        ResizeMode="CanResizeWithGrip"
        Closed="Window_Closed">

    <Window.Resources>
        <BooleanToVisibilityConverter x:Key="B2VConverter"/>
    </Window.Resources>

    <mah:MetroWindow.LeftWindowCommands>
        <mah:WindowCommands>
            <Button Click="LaunchTwitterSite" ToolTip="Twitter">
                <iconPacks:PackIconModern Width="17"
                                  Height="17"
                                  Kind="SocialTwitter"/>
            </Button>

            <Button Click="LaunchTwitchSite" ToolTip="Twitch">
                <iconPacks:PackIconModern Width="17"
                                  Height="17"
                                  Kind="Video"/>
            </Button>
        </mah:WindowCommands>
    </mah:MetroWindow.LeftWindowCommands>

    <mah:MetroWindow.RightWindowCommands>
        <mah:WindowCommands>
            <Button Click="AboutFlyout" Content="About">
                <Button.ContentTemplate>
                    <DataTemplate>
                        <StackPanel Orientation="Horizontal">
                            <iconPacks:PackIconModern Width="22"
                                        Height="22"
                                        VerticalAlignment="Center"
                                        Kind="Information" />
                            <TextBlock Margin="4 0 0 0"
                         VerticalAlignment="Center"
                         Text="{Binding}" />
                        </StackPanel>
                    </DataTemplate>
                </Button.ContentTemplate>
            </Button>
        </mah:WindowCommands>
    </mah:MetroWindow.RightWindowCommands>

    <mah:MetroWindow.Flyouts>
        <mah:FlyoutsControl>
            <mah:Flyout x:Name="aboutFlyout" Header="About" Position="Right" Width="400">
                <ScrollViewer Margin="10">
                    <StackPanel Orientation="Vertical">
                        <TextBlock>Author: TheOnlyDroid</TextBlock>
                        <TextBlock>Website: https://theonlydroid.com</TextBlock>
                        <TextBlock>Twitter: https://twitter.com/theonlydroid</TextBlock>
                        <TextBlock>Twitch: https://twitch.tv/theonlydroid</TextBlock>
                        <TextBlock>MahApps Metro: https://mahapps.com</TextBlock>
                        <TextBlock>Newtonsoft JSON: https://www.newtonsoft.com/json</TextBlock>
                        <TextBlock>Sounds: Various Authors (config.json)</TextBlock>
                    </StackPanel>
                </ScrollViewer>
            </mah:Flyout>
        </mah:FlyoutsControl>
    </mah:MetroWindow.Flyouts>

    <Grid>
        <Grid.ColumnDefinitions>
            <ColumnDefinition />
            <ColumnDefinition Width="0*"/>
        </Grid.ColumnDefinitions>

        <StackPanel Grid.Column="0" Background="White">
            <Grid Margin="5"/>
            <Grid Grid.Column="1" Margin="5">
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="1*" />
                    <ColumnDefinition Width="1*" />
                </Grid.ColumnDefinitions>

                <Grid Grid.Column="1">
                    <Grid.RowDefinitions>
                        <RowDefinition Height="1*" />
                        <RowDefinition Height="1*" />
                    </Grid.RowDefinitions>
                    <TextBlock Grid.Row="0">Eye Correction</TextBlock>
                    <StackPanel Orientation="Horizontal" x:Name="eyeCorrectionMode" Grid.Row="1">
                        <RadioButton GroupName="eyeMode" HorizontalAlignment="Center" Name="leftTrans" Style="{DynamicResource radioEyeControlLeft}" Content="LEFT" HorizontalContentAlignment="Center"/>
                        <RadioButton GroupName="eyeMode" HorizontalAlignment="Center" IsChecked="True" Name="bothTrans" Style="{DynamicResource radioEyeControlBoth}" Margin="5,0,5,0" Content="BOTH" HorizontalContentAlignment="Center"/>
                        <RadioButton GroupName="eyeMode" HorizontalAlignment="Center" Name="rightTrans" Style="{DynamicResource radioEyeControlRight}" Content="RIGHT" HorizontalContentAlignment="Center"/>
                    </StackPanel>
                </Grid>

                <Grid Grid.Column="0">
                    <Grid.RowDefinitions>
                        <RowDefinition Height="17*" />
                        <RowDefinition Height="60*" />
                    </Grid.RowDefinitions>
                    <TextBlock Grid.Row="0">Blink Mode</TextBlock>
                    <mah:ToggleSwitch Margin="5,19,0,9" x:Name="blinkyMcBlink" IsOn="False" OnContent="Termination Mode" OffContent="Blink Mode" VerticalContentAlignment="Center" VerticalAlignment="Center" Grid.Row="1" Height="32" Width="169" />
                </Grid>


            </Grid>

            <Grid Grid.Column="1">
                <Grid.RowDefinitions>
                    <RowDefinition Height="1*" />
                    <RowDefinition Height="1*" />
                </Grid.RowDefinitions>
                <TextBlock Text="Hotkey" VerticalAlignment="Center" Margin="8,0,0,0" FontSize="15"></TextBlock>
                <mah:HotKeyBox x:Name="key" KeyUp="OnKeybindChanged" Margin="5" Grid.Row="1" ></mah:HotKeyBox>
            </Grid>


            <Grid Margin="5">


            </Grid>

            <GridSplitter Height="3" Margin="5" Background="#FFF0F0F0" />
            <Grid Visibility="{Binding ElementName=blinkyMcBlink,Path=IsOn, Converter={StaticResource B2VConverter}}" Margin="5">
                <StackPanel Orientation="Vertical">
                    <Grid Margin="5">
                        <TextBlock>Audio Settings</TextBlock>
                    </Grid>
                    <ComboBox x:Name="availableSounds" Margin="5" SelectedIndex="0"/>
                    <Grid Grid.Column="1">
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="1*" />
                            <ColumnDefinition Width="1*" />
                        </Grid.ColumnDefinitions>

                        <Button Margin="3" Grid.Column="1" x:Name="refreshSounds" Click="refreshSounds_Click">
                            <Image Source="images/Refresh.png" Width="25"></Image>
                        </Button>

                        <Button Margin="5" Grid.Column="0" x:Name="testPlay" Click="testPlay_Click">
                            <Image Source="images/Play.png" Width="25"></Image>
                        </Button>
                    </Grid>
                </StackPanel>
            </Grid>

            <GridSplitter Height="3" Margin="5" Background="#FFF0F0F0" Visibility="{Binding ElementName=blinkyMcBlink,Path=IsOn, Converter={StaticResource B2VConverter}}" />

            <Grid Margin="5">
                <TextBlock>Start Detecting</TextBlock>
            </Grid>

            <Grid Margin="5">
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="43*"/>
                    <ColumnDefinition Width="539*"/>
                </Grid.ColumnDefinitions>
            </Grid>
            <Button x:Name="startButton" Content="Start" Margin="5" Height="35" Click="WorkerStart_Click"/>
            <GridSplitter Height="3" Margin="5" Background="#FFF0F0F0" />
            <Viewbox HorizontalAlignment="Stretch" VerticalAlignment="Stretch">
                <StackPanel>
                    <Grid Margin="5">
                        <TextBlock>Log</TextBlock>
                    </Grid>
                    <ScrollViewer Margin="5" Height="100" Width="400">
                        <RichTextBox Margin="5" x:Name="eventLog" IsEnabled="False"/>
                    </ScrollViewer>
                </StackPanel>
            </Viewbox>
            <Grid>

            </Grid>

        </StackPanel>
    </Grid>
</mah:MetroWindow>
