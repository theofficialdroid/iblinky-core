﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Tobii.Blinky.Services
{
    public static class VersionCheck
    {
        public static bool CheckAsync(string url, string version, int build)
        {
            Debug.WriteLine($"[DEBUG::UPDATE::CHECK]: Checking for update from current version ({version}) from {url}");

            try
            {
                using (var webClient = new System.Net.WebClient())
                {
                    var json = webClient.DownloadString(url);
                    dynamic array = JsonConvert.DeserializeObject(json);

                    Debug.WriteLine($"[DEBUG::UPDATE::CHECK]: {version} <=> {array["version"]}");
                    Debug.WriteLine($"[DEBUG::UPDATE::CHECK]: {( version.Equals( array["version"] ) ? "equal." : "not equal.")}");

                    if (!version.Equals(array["version"]) && array["build"] > build)
                        return false;
                }
            } catch(Exception ex)
            {
                Debug.WriteLine("[DEBUG::UPDATE::CHECK]: Unable to continue to parse update data");
            }

            return true;
        }
    }
}
