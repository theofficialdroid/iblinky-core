﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows;
using System.Windows.Media;
using System.IO;
using System.Diagnostics;
using System.ComponentModel;
using System.Collections.Generic;
using MahApps.Metro.Controls;
using System.Windows.Input;
using System.Linq;
using System.Windows.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace Tobii.Blinky
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        Thread tobiiThread;
        MagicalEyes magicalEyes = new MagicalEyes();
        Tobii.InteractionLib.IInteractionLib intlib;

        bool started = false;
        bool blinkDetected = false;
        string eyeMode = String.Empty;
        string basePath = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
        ObservableCollection<Models.Sound> terminationSoundCollection = new ObservableCollection<Models.Sound>();

        Color eyeStart = Color.FromRgb(0, 190, 255);
        Color eyeBlink = Color.FromRgb(230, 126, 34);
        Color eyeTerminated = Color.FromRgb(231, 76, 60);

        Services.LowLevelKeyboardListener _bindListener = new Services.LowLevelKeyboardListener();
        Key customBind = Key.F9;

        public MainWindow()
        {
            //eventLog.AppendText($"[{DateTime.Now}] Initialising iBlinky");

            DataContext = this;
            InitializeComponent();

            LoadConfig();

            //eventLog.AppendText($"[{DateTime.Now}] Hooking keyboard process for hotkey.");
            _bindListener.OnKeyPressed += _bindListener_OnKeyPressed;
            _bindListener.HookKeyboard();

            // Assign item source directly, {Binding terminationSoundCollection, Path=terminationSoundCollection}/{Binding terminationSoundCollection} failed to work
            availableSounds.ItemsSource = terminationSoundCollection;
            availableSounds.DisplayMemberPath = "Name";

            LoadAvailableSounds();
            startButton.Content = $"Start (HOTKEY: {customBind.ToString()})";
            magicalEyes.Show();
        }

        private void _bindListener_OnKeyPressed(object sender, Services.KeyPressedArgs e)
        {
            Debug.WriteLine($"[DEBUG::KEYBIND]: KEY PRESSED {e.KeyPressed.ToString()}");
            if (e.KeyPressed == customBind)
            {
                WorkerStart_Click(null, null);
            }
        }

        private void Intlib_GazePointDataEvent(InteractionLib.GazePointData gazePointData)
        {
            if(blinkDetected)
            {
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    if (magicalEyes.Eyecolour.Color == eyeBlink)
                    {
                        magicalEyes.Eyecolour = new SolidColorBrush(eyeStart);
                    }
                }));

                if (gazePointData.x == -1 && gazePointData.y == -1)
                {
                    blinkDetected = false;
                    BlinkyMcBlinkDoesBlink();
                }
            }
        }

        private void Intlib_GazeOriginDataEvent(InteractionLib.GazeOriginData gazeOriginData)
        {
            if (started)
            {
                if (eyeMode == "leftTrans")
                {
                    Debug.WriteLine("[DEBUG::BLINK]: leftTrans detected.");
                    if (gazeOriginData.leftValidity == InteractionLib.Validity.Invalid)
                    {
                        Debug.WriteLine("[DEBUG::BLINK]: possible blink, executing.");
                        blinkDetected = true;
                    }
                }
                if (eyeMode == "rightTrans")
                {
                    Debug.WriteLine("[DEBUG::BLINK]: rightTrans detected.");
                    if (gazeOriginData.rightValidity == InteractionLib.Validity.Invalid)
                    {
                        Debug.WriteLine("[DEBUG::BLINK]: possible blink, executing.");
                        blinkDetected = true;
                    }
                }
                if (eyeMode == "bothTrans")
                {
                    Debug.WriteLine("[DEBUG::BLINK]: bothTrans detected.");
                    if (gazeOriginData.leftValidity == InteractionLib.Validity.Invalid && gazeOriginData.rightValidity == InteractionLib.Validity.Invalid)
                    {
                        Debug.WriteLine("[DEBUG::BLINK]: possible blink, executing.");
                        blinkDetected = true;
                    }
                }
            }
        }

        private void BlinkyMcBlinkDoesBlink()
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                if (blinkyMcBlink.IsOn == false)
                {
                    magicalEyes.Eyecolour = new SolidColorBrush(eyeBlink);
                }
                else
                {
                    terminationSoundCollection[availableSounds.SelectedIndex].player.Play();
                    magicalEyes.Eyecolour = new SolidColorBrush(eyeTerminated);
                    WorkerStart_Click(null, null);
                }
            }));
        }

        private void WorkerStart_Click(object sender, RoutedEventArgs e)
        {
            if (!started)
            {
                eventLog.AppendText($"[{DateTime.Now}] Started Detecting Blinks\n");
                startButton.Content = $"STOP (HOTKEY: {customBind.ToString()})";
                intlib = Tobii.InteractionLib.InteractionLibFactory.CreateInteractionLib(InteractionLib.FieldOfUse.Interactive);


                intlib.GazePointDataEvent += Intlib_GazePointDataEvent;
                intlib.GazeOriginDataEvent += Intlib_GazeOriginDataEvent;


                eyeMode = eyeCorrectionMode.Children.OfType<RadioButton>()
                 .FirstOrDefault(r => r.IsChecked.HasValue && r.IsChecked.Value).Name;

                Application.Current.Dispatcher.Invoke(new Action(() => {
                    magicalEyes.Eyecolour = new SolidColorBrush(eyeStart);
                }));

                started = true;
                ThreadStart work = delegate () {
                    while (started)
                    {
                        intlib.WaitAndUpdate();
                    }
                };
                tobiiThread = new Thread(work);
                tobiiThread.Start();
            }
            else
            {
                eventLog.AppendText($"[{DateTime.Now}] Stopped Detecting Blinks \n");
                startButton.Content = $"Start (HOTKEY: {customBind.ToString()})";
                started = false;

                try
                {
                    intlib.Dispose();
                    tobiiThread.Abort();
                } catch (Exception ex)
                {
                    eventLog.AppendText($"[{DateTime.Now}] Error while exiting tracking thread: {e.ToString()}.\n");
                }

            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if(intlib != null)
                intlib.Dispose();

            if (tobiiThread != null)
                tobiiThread.Abort();

            _bindListener.UnHookKeyboard();
            magicalEyes.Close();
        }

        private void testPlay_Click(object sender, RoutedEventArgs e)
        {
            terminationSoundCollection[availableSounds.SelectedIndex].player.Play();
        }

        private void refreshSounds_Click(object sender, RoutedEventArgs e)
        {
            terminationSoundCollection.Clear();
            LoadAvailableSounds();
            availableSounds.SelectedIndex = 0;
        }

        private void LoadConfig()
        {
            eventLog.AppendText($"[{DateTime.Now}] Loading Configuration.\n");
            try
            {
                using (StreamReader reader = File.OpenText(basePath + @"\configuration\config.json"))
                { 
                    JObject o = (JObject)JToken.ReadFrom(new JsonTextReader(reader));

                    try
                    {
                        if (o["check_url"] != null)
                        {
                            if (!Services.VersionCheck.CheckAsync((string)o["check_url"], (string)o["version"], (int)o["build"]))
                            {
                                MessageBox.Show($"You're running an outdated version of iBlinky\n\nYou may download the latest version from the following url\n{(string)o["download_url"]}");
                            }
                              
                        } else { 
                            Debug.WriteLine("[DEBUG::UPDATE]: No check_url present.");  
                        }
                    } catch (Exception vcE)
                    {
                        eventLog.AppendText($"[{DateTime.Now}] Unable to check version, please ensure configuration is not malformed. \n");
                    }


                    if(o["colours"].HasValues)
                    {
                        try
                        {
                            eyeStart = Color.FromRgb((byte)o["colours"]["started"]["r"], (byte)o["colours"]["started"]["g"], (byte)o["colours"]["started"]["b"]);
                            eyeBlink = Color.FromRgb((byte)o["colours"]["blinked"]["r"], (byte)o["colours"]["blinked"]["g"], (byte)o["colours"]["blinked"]["b"]);
                            eyeTerminated = Color.FromRgb((byte)o["colours"]["ended"]["r"], (byte)o["colours"]["ended"]["g"], (byte)o["colours"]["ended"]["b"]);

                            Debug.WriteLine($"[DEBUG::CONFIG::START]: {(byte)o["colours"]["started"]["r"]}, {(byte)o["colours"]["started"]["g"]}, {(byte)o["colours"]["started"]["b"]}");
                            Debug.WriteLine($"[DEBUG::CONFIG::BLINK]: {(byte)o["colours"]["blinked"]["r"]}, {(byte)o["colours"]["blinked"]["g"]}, {(byte)o["colours"]["blinked"]["b"]}");
                            Debug.WriteLine($"[DEBUG::CONFIG::ENDED]: {(byte)o["colours"]["ended"]["r"]}, {(byte)o["colours"]["ended"]["g"]}, {(byte)o["colours"]["ended"]["b"]}");

                            eventLog.AppendText($"[{DateTime.Now}] Loaded colour configuration.\n");
                        } catch(Exception ec) {
                            eventLog.AppendText($"[{DateTime.Now}] Unable to load colour values, using defaults. \n");
                        }
                    }
                }
            } catch (Exception ex) {
                eventLog.AppendText($"[{DateTime.Now}] Unable to load configuration file; using default values. \n");
            }

        }

        private void LoadAvailableSounds()
        {
            eventLog.AppendText($"[{DateTime.Now}] Loading available sounds from {basePath}\n");
            try
            {
                if (Directory.Exists(basePath + @"\sounds"))
                {
                    string[] files = Directory.GetFiles(basePath, @"sounds\*.wav", SearchOption.TopDirectoryOnly);
                    foreach (string s in files)
                    {
                        eventLog.AppendText($"[{DateTime.Now}] Loaded sound '{s.Remove(0, (basePath + @"\sounds\").Length)}' for use.\n");
                        terminationSoundCollection.Add(new Models.Sound()
                        {
                            Name = s.Remove(0, (basePath + @"\sounds\").Length),
                            path = s,
                            player = new System.Media.SoundPlayer(s)
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Unable to load sounds for use with iBlinky, this should not affect the use of the application; however sounds will be unavailable\n\nException Message: {ex.Message}");
            }
        }

        private void LaunchTwitterSite(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.twitter.com/theonlydroid");
        }

        private void LaunchTwitchSite(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.twitch.tv/theonlydroid");
        }

        private void AboutFlyout(object sender, RoutedEventArgs e)
        {
            aboutFlyout.IsOpen = true;
        }

        private void OnKeybindChanged(object sender, System.Windows.Input.KeyEventArgs e)
        {
            customBind = e.Key;
            if(started)
                startButton.Content = $"STOP (HOTKEY: {customBind.ToString()})";
            else
                startButton.Content = $"START (HOTKEY: {customBind.ToString()})";
        }
    }
}
